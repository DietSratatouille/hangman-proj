//Hangman is a game for two people in which one "player" (in our case the computer)
// thinks of a word and then draws a blank line on the page for each letter in the word.
// The second player then tries to guess letters that might be in the word.
//        +---+
//        O   |
//        |    |
//        |    |
//        ===
//Missed letters:
//
//_ _ _
//
//Guess a letter.

import java.util.Random;
import java.util.Scanner;

public class Main {
//    static String[] wordOpts = {"cat","hat", "bat", "gat","tat"};
//    //int n = wordOpts.length;
//    static char [] guesses;
//    static char [] wrong;

    public static void printArr(char [] chArr) {      //for ch index in the length of the array, print each char
        for (int i = 0; i < chArr.length; i ++) {
//            System.out.println("        +---+\n" +
//                    "            |\n" +
//                    "            |\n" +
//                    "            |\n" +
//                    "           ===");

            System.out.print(chArr[i]);

        }
        System.out.println();
    }   //end of printArr() method
//______________________________________________________________________________________________________________________

    public static boolean isTheWordGuessed(char[] chArr) {  //checks to see whether a '_' char is present and returns boolean value
        boolean cond = true;
        for (int i = 0; i <chArr.length; i++){
            if (chArr[i] == '_') {
                cond = false;
            }
        }
        return cond;
    }  //end of isTheWordGuessed() method

    //__________________________________________________________________________________________________________________


    //begin main driver

    public static void main(String[] args) {

        //starting variables
        String[] wordOpts = {"cat","hat", "bat", "gat","tat"};
        //int n = wordOpts.length;
        //char [] guesses;
        //char [] wrong;


//         String[] wordOpts = {"cat","hat", "bat", "gat","tat"};
//        //int n = wordOpts.length;
//       char [] userGuess= new char[]{};
//        char [] wrong;
        System.out.println("WELCOME TO HANGMAN");
        System.out.println("        +---+\n" +
                "        O   |\n" +
                "        |   |\n" +
                "        |   |\n" +
                "       ===");


        System.out.println("Bang a key to begin");
        boolean stillPlaying = false;

        //System.out.println("Would you like to play? Key y/n");
        Scanner s1 = new Scanner(System.in);
        String userInput_1 = s1.nextLine();

//end of starting variables
//______________________________________________________________________________________________________________________


        while (!stillPlaying ){
            //while (!userInput_1.equals("n")) {

            //}
            System.out.println("Game is on buddy!   o.O");
            Random rand = new Random();
            int randNum = rand.nextInt(wordOpts.length);
            char[] wordToGuess = wordOpts[randNum].toCharArray();
            int attempts = wordToGuess.length;

            char [] playerGuess = new char[attempts];

            for(int i = 0; i < playerGuess.length; i++) {
                playerGuess[i] = '_';

            }

            boolean correctGuess = false;
            int userTries = 0;

            while(!correctGuess && userTries != attempts ) {
                System.out.println("Your current guesses: ");
                printArr(playerGuess);
                System.out.printf("%d attempts left!" , attempts - userTries);
                System.out.println(" Enter one character for your guess");
                Scanner s2 = new Scanner(System.in);
                char inputGuess = s2.nextLine().charAt(0);
                //char guessChar = officialGuess.charAt(0);
                userTries++;

                //cases here?
                //if          //if statement to know when to add hangman body parts
                switch (userTries){
                    case (1):
                        System.out.println("        +---+\n" +
                                "         o  |\n" +
                                "            |\n" +
                                "            |\n" +
                                "           ===");
                        break;
                    case(2):
                        System.out.println("        +---+\n" +
                                "         o  |\n" +
                                "         |  |\n" +
                                "            |\n" +
                                "           ===");
                        break;

                    case(3):
                        System.out.println("        +---+\n" +
                                "         o  |\n" +
                                "         |  |\n" +
                                "         |  |\n" +
                                "           ===");
                        break;
                }

                if (inputGuess == '_') {
                    correctGuess = true;
                    stillPlaying = false;

                }

                else {
                    for (int i= 0; i<wordToGuess.length; i++) {
                        //cases here?

                        if (wordToGuess[i] == inputGuess) {
                            playerGuess[i] = inputGuess;
                            System.out.println(playerGuess[i]);
                        }

                        //cases go for hangman part additions below
//                        switch (userTries){
//                            case (1):
//                                System.out.println("        +---+\n" +
//                                        "         o  |\n" +
//                                        "            |\n" +
//                                        "            |\n" +
//                                        "           ===");
//                                break;
//                            case(2):
//                                System.out.println("        +---+\n" +
//                                        "         o  |\n" +
//                                        "         |  |\n" +
//                                        "            |\n" +
//                                        "           ===");
//                                break;
//
//                            case(3):
//                                System.out.println("        +---+\n" +
//                                        "         o  |\n" +
//                                        "         |  |\n" +
//                                        "         |  |\n" +
//                                        "           ===");
//                                break;
//                        }

                    }

                    if (isTheWordGuessed(playerGuess)){
                        correctGuess = true;
                        String str = "Here's";  //practice for interpolation
                        String interpolMe = String.format("%s your trophy!", str); //practice for interpolation
                        System.out.println("Gg's lil man, wow! " + interpolMe);

                    }
                }




            } //end of while loop2 (correctGuess && ...)
//______________________________________________________________________________________________________________________
            if(!correctGuess) {
                String concat = "Sorry!";  //concat practice
                System.out.println("Outta guesses pal.. " + concat);   //concat practice
            }

            System.out.println("Wanna play again?? (Key yes/no)");
            String userCont = s1.nextLine();
            userCont.toLowerCase();
            if (userCont.equals("no")){
                stillPlaying = false;

                break;

            }


        }  //end of while loop1 (stillPlaying)
//______________________________________________________________________________________________________________________

        System.out.println("GG's bum... See ya.");

    }
}
